# -*- coding:utf-8 -*-
from __future__ import absolute_import, print_function, division
import skimage
import numpy as np
from skimage.feature import blob_log
from skimage.draw import circle_perimeter_aa
from matplotlib import pyplot as plt

class AllSkyCamAnalysis:

	def __init__(self, img, camera_props):
		self.raw = img
		self.camera_props = camera_props
		self._process()

	def _process(self):
		self._init_recognition_radii()
		self._init_pixel_mapping()	
		self._init_zenith()
		self._init_direction_north()
		self._init_horizon_mask_from_raw()
		self._init_sky_without_horizon()
		self._init_sky_gray()	
		self._init_sky_minus_median_on_star_scale()
		self._init_sky_minus_median_on_moon_scale()

		self._find_star_candidates()
		self._find_moon_candidates()

		self._init_total_response()
		self._init_moon_candidate_expected_responses()
		self._init_moon_candidate_responses()

		self._find_moon()
		self._find_stars()
	
	def _init_recognition_radii(self):
		self.star_radius_in_deg = self.camera_props['star_radius_deg']	
		self.moon_radius_in_deg = self.camera_props['moon_radius_deg']	

	def _init_pixel_mapping(self):
		self.pixel_per_deg = self.camera_props['pixel_per_deg']

	def _init_direction_north(self):
		self.direction_north_deg = self.camera_props['direction_north_deg']

	def	_init_zenith(self):
		self.zenith_col = self.camera_props['zenith_pixel_col']
		self.zenith_row = self.camera_props['zenith_pixel_row']

	def _init_horizon_mask_from_raw(self):
		self.horizon_mask = self._create_round_mask(
			self.raw,
			self.zenith_row,
			self.zenith_col,
			self.pixel_per_deg*90.0
		)

	def _init_sky_without_horizon(self):
		self.sky = np.copy(self.raw)
		self.sky[self.horizon_mask] = 0

	def _init_sky_gray(self):
		self.sky_gray = self._rgb2gray_8bit(
			self.sky
		)		

	def _init_sky_minus_median_on_star_scale(self):
		star_nbh_radius = np.ceil(self.pixel_per_deg*self.star_radius_in_deg)

		self.sky_minus_median_star = self._get_img_minus_local_median(
			self.sky_gray, 
			self.horizon_mask,
			star_nbh_radius
		)		

	def _init_sky_minus_median_on_moon_scale(self):
		moon_nbh_radius = np.ceil(self.pixel_per_deg*self.moon_radius_in_deg)

		self.sky_minus_median_moon = self._get_img_minus_local_median(
			self.sky_gray, 
			self.horizon_mask,
			moon_nbh_radius
		)

	def _find_star_candidates(self):
		self.star_candidates = self._find_highlights(
			self.sky_minus_median_star, 
			self.star_radius_in_deg
		)

	def _find_moon_candidates(self):
		self.moon_candidates = self._find_highlights(
			self.sky_minus_median_moon, 
			self.moon_radius_in_deg
		)

	def _create_mask_from_blob(self, blob):
		y, x, r = blob
		return self._create_round_mask(self.sky,y,x,r)
	
	def _create_round_mask(self,img,x,y,radius):
		X, Y = np.ogrid[:img.shape[0],:img.shape[1]]
		return (X-x)**2.0 + (Y-y)**2.0 > radius**2.0

	def _find_highlights(self, img, radius_in_deg):

		try:
			radius_in_px = np.ceil(np.sqrt(2.0)*self.pixel_per_deg*radius_in_deg)

			if radius_in_px < 2:
				print('AllSkyCam Analysis Warning: find highlights, pixel radius below 2.')


			blobs = skimage.feature.blob_log(
				img, 
				max_sigma=radius_in_px*3, 
				min_sigma=radius_in_px,
				num_sigma=5, 
				threshold=0.005
			)
			blobs[:,2] = blobs[:,2]*np.sqrt(2.0)
			return blobs
		except TypeError:
			print('_find_highlights failed, there are no highlights in this image')
			return np.empty(shape=(0,3))
	
	def _init_total_response(self):
		self.total_response = float(np.sum(np.sum(self.sky_gray)))

	def _expected_moon_candidate_responses(self, radius):
		return 255.0*0.5*np.pi*radius**2.0

	def _init_moon_candidate_expected_responses(self):
		self.moon_candidate_expected_responses = self._expected_moon_candidate_responses(self.moon_candidates[:,2].astype('float'))

	def _init_moon_candidate_responses(self):
		self.moon_candidate_responses = np.zeros(self.moon_candidates.shape[0])

		i=0
		for blob in self.moon_candidates:
			blob_mask = self._create_mask_from_blob(blob)

			blob_only = np.copy(self.sky_gray)
			blob_only[blob_mask]=0

			blob_flux = np.sum(np.sum(blob_only))
			self.moon_candidate_responses[i] = float(blob_flux)
			i=i+1

	def _find_moon(self):
		self.moon = np.empty(shape=(0,3))
		i = np.argmax(self.moon_candidate_responses)
		response = float(self.moon_candidate_responses[i])
		ratio_of_total_response = response/self.total_response

		if(ratio_of_total_response > 0.05 and response > self.moon_candidate_expected_responses[i]):
			self.moon = np.array([self.moon_candidates[i,:]])

	def _find_stars(self):
		self.stars = np.empty(shape=(0,3))

		for star_candidate in self.star_candidates:
			#sx, sy, sr = star_candidate

			#in_any_moon_candidate = False

			#for moon_candidate in self.moon_candidates:
			#	mx, my, mr = moon_candidate

			#	dx = float(mx)-float(sx)
			#	dy = float(my)-float(sy)

			#	if dx**2.0 + dy**2.0 < float(mr)**2.0:
			#		in_any_moon_candidate = True

			#if not in_any_moon_candidate:
			self.stars = np.vstack((self.stars, star_candidate))

	def _show_stars(self):
		fig, ax = plt.subplots(1, 1)
		ax.set_title('stars: ' + str(self.stars.shape[0]) + ' moon like: ' + str(self.moon_candidates.shape[0]))
		ax.imshow(self.sky, interpolation='nearest')

		for blob in self.star_candidates:
			y, x, r = blob
			c = plt.Circle((x, y), r, color='yellow', linewidth=2, fill=False)
			ax.add_patch(c)

		for blob in self.stars:
			y, x, r = blob
			c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
			ax.add_patch(c)

		for blob in self.moon_candidates:
			y, x, r = blob
			c = plt.Circle((x, y), r, color='blue', linewidth=2, fill=False)
			ax.add_patch(c)

		if self.moon.size >0:
			y, x, r = self.moon
			c = plt.Circle((x, y), r, color='green', linewidth=2, fill=False)
			ax.add_patch(c)

		plt.show()

	def _get_img_minus_local_median(self, img, veto_mask, nbh_radius):
		# create mask
		X, Y = np.ogrid[:nbh_radius*2.0,:nbh_radius*2.0]
		nbh_mask = (X-nbh_radius)**2.0 + (Y-nbh_radius)**2.0 < nbh_radius**2.0

		#convolute local median
		img_local_median = skimage.filters.rank.median(
			image=img,
			mask=np.invert(veto_mask),
			selem=nbh_mask
		)

		#substrsact local median
		img = (img.astype('float64') - img_local_median.astype('float64'))

		#remove artefacts
		artefact_mask = img < 0.0 
		img[artefact_mask] = 0.0
		img = img.astype('uint8')
		return img

	def _rgb2gray_8bit(self, img):
		return (255.0*skimage.color.rgb2gray(img)).astype('uint8')	

	def get_stars_down_to_Zd_in_deg(self, max_Zd_in_deg):
		max_Zd_in_pix = self.pixel_per_deg*max_Zd_in_deg

		dist2zenith = np.hypot(
			self.stars[:,0]-self.zenith_row, 
			self.stars[:,1]-self.zenith_col
		)

		indices_of_stars_belox_Zd_limit = np.where( dist2zenith < max_Zd_in_pix)[0];	
		return self.stars[indices_of_stars_belox_Zd_limit]

	def _show_with_blobs(self, blobs, title):
		fig, ax = plt.subplots(1, 1)
		ax.set_title(title)
		ax.imshow(self.sky, interpolation='nearest')

		for blob in blobs:
			y, x, r = blob
			c = plt.Circle((x, y), r, color='yellow', linewidth=2, fill=False)
			ax.add_patch(c)

		plt.show()


	def get_image_with_blobs(self, blobs):
		overlay = np.copy(self.sky)
		blobs.astype('int')

	 	for blob in blobs:
			y, x, r = blob
			rr, cc, val = circle_perimeter_aa(int(y), int(x), int(r), shape=overlay.shape)
			overlay[rr, cc] = [255,0,0]

		return overlay

	def _Az_Zd_2_row_col(self, Az_deg, Zd_deg):

		Zd_pix = Zd_deg*self.pixel_per_deg
		Az_north = Az_deg + self.direction_north_deg
		Az_north_rad = np.deg2rad(Az_north)

		row = self.zenith_row + Zd_pix*np.cos(Az_north_rad)
		col = self.zenith_col + Zd_pix*np.sin(Az_north_rad)

		return row, col

	def get_image_with_blobs_at_Az_Zd(self, Az_deg, Zd_deg, radius_deg=1.0):

		overlay = np.copy(self.sky)
		radius_pix = self.pixel_per_deg*radius_deg

		row, col = self._Az_Zd_2_row_col(Az_deg, Zd_deg)
		rr, cc, val = circle_perimeter_aa(int(row), int(col), int(radius_pix), shape=overlay.shape)
		overlay[rr, cc] = [255,0,0]

		return overlay