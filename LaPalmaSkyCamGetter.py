#!/usr/bin/env python
from __future__ import absolute_import, print_function, division
from skimage.io import imread
from skimage.io import imsave
from skimage.io import imshow
from skimage.io import show
from datetime import datetime
from pytz import UTC
from skimage.transform import resize 
import skimage
from numpy import hstack
from numpy import vstack
from numpy import zeros
import numpy as  np
from AllSkyCamAnalysis import AllSkyCamAnalysis
from matplotlib import pyplot as plt
import matplotlib.offsetbox as offsetbox
import io as InOut
import smart_fact_crawler as sfc

def timestamp():
	return datetime.utcnow().strftime('%Y%m%d_%H%M%S') 

def scale_image_to_apsect4to3_and_rows(img, rows):
	return (
		skimage.transform.resize(img, (rows, int(rows/0.75)))*255
	).astype('uint8')

def get_star_bar(camera_name, star_count, star_count_in_good_night, moon_count, rows):

	col = 'red'
	if star_count > 0.75*star_count_in_good_night:
		col = 'green'

	my_dpi = 96

	hi = .5
	wi = star_count_in_good_night*1.1

	fig = plt.figure(figsize=(640/my_dpi, rows/my_dpi), dpi=my_dpi)
	plt.barh(0.0, star_count, align='center', color=col)
	plt.plot([star_count_in_good_night,star_count_in_good_night],[-hi/2.0,+hi/2.0],color="blue")
	ax = plt.gca()
	ax.yaxis.set_visible(False)
	titobj = ax.set_title(camera_name+' '+str(star_count)+' stars')
	plt.setp(titobj,color = 'r')
	if moon_count > 0:
		xlabelobj = ax.set_xlabel('Moon')
		plt.setp(xlabelobj,color = 'r')
	ax.set_aspect(2)
	ax.axis((0, wi, -hi/2.0, hi/2.0))
	ax.set_aspect(0.1*wi/hi)
	ax.spines['top'].set_color('none')
	ax.spines['right'].set_color('none')
	ax.spines['left'].set_color('none')
	ax.xaxis.set_ticks_position('bottom')
	plt.setp(plt.getp(ax, 'xticklabels'), color='r')

	buf = InOut.BytesIO()
	plt.savefig(buf, format='png', dpi=my_dpi, transparent=False, frameon=False, facecolor='black', edgecolor='none')
	buf.seek(0)
	
	return (imread(buf)[:,:,0:3]).astype('uint8')

def get_info_image(text, rows=1080):
	my_dpi = 96

	fig = plt.figure(figsize=(640/my_dpi, rows/my_dpi), dpi=my_dpi)
	ax = plt.gca()
	ax.axis('off')
	plt.setp(plt.getp(ax, 'xticklabels'), color='r')
	
	# Draw text box.
	ob = offsetbox.AnchoredText(text, pad=1, loc=6, prop=dict(size=13))
	ob.patch.set(alpha=0.85)
	ax.add_artist(ob)


	buf = InOut.BytesIO()
	plt.savefig(buf, format='png', dpi=my_dpi, transparent=False, frameon=False, facecolor='black', edgecolor='none')
	buf.seek(0)
	
	return (imread(buf)[:,:,0:3]).astype('uint8')


all_sky_images =  [
	[
		'FACT_AllSky', 
		'http://fact-project.org/cam/skycam.php', 
		{	
			'pixel_per_deg':2.900,
			'zenith_pixel_col':357, 
			'zenith_pixel_row':304,
			'direction_north_deg':15.0, 
			'star_radius_deg':0.35,
			'moon_radius_deg':7.0},
		{'star_count_in_good_night': 350}
	], 
	[
		'GTC_AllSky', 
		'http://www.gtc.iac.es/multimedia/netcam/camaraAllSky.jpg',
		{
			'pixel_per_deg':3.466,
			'zenith_pixel_col':339,
			'zenith_pixel_row':236,
			'direction_north_deg':210.0,
			'star_radius_deg':0.3,
			'moon_radius_deg':7.0},
		{'star_count_in_good_night': 840}
	],
	[
		'MAGIC_AllSky', 
		'http://www.magic.iac.es/site/weather/AllSkyCurrentImage.JPG',
		{
			'pixel_per_deg':3.733, 
			'zenith_pixel_col':378,
			'zenith_pixel_row':234,
			'direction_north_deg':177.5,
			'star_radius_deg':0.275,
			'moon_radius_deg':7.0},
		{'star_count_in_good_night': 950}
	],
]

Zd_limit_in_deg = 75.0

total_rows = 1080.0
allsky_rows = 480.0
overlay_rows = 240.0
info_rows = total_rows - allsky_rows -overlay_rows
allsky_stack = zeros(shape=(allsky_rows,0,3)).astype('uint8')
info_stack = zeros(shape=(info_rows,0,3)).astype('uint8')
over_stack = zeros(shape=(overlay_rows,0,3)).astype('uint8')

smf = sfc.SmartFact()

for img_desc in all_sky_images:
	raw_img = imread(img_desc[1])
	cam_analyser = AllSkyCamAnalysis(raw_img, img_desc[2])
	stars_down_to_Zd_limit = cam_analyser.get_stars_down_to_Zd_in_deg(Zd_limit_in_deg)
	number_of_stars_below_Zd_limit = stars_down_to_Zd_limit.shape[0]

	over_star = cam_analyser.get_image_with_blobs(stars_down_to_Zd_limit)
	over_star = scale_image_to_apsect4to3_and_rows(over_star,allsky_rows/2)
	over_moon = cam_analyser.get_image_with_blobs(cam_analyser.moon)
	over_moon = scale_image_to_apsect4to3_and_rows(over_moon,allsky_rows/2)
	over = hstack((over_star, over_moon))

	Az = smf.drive()['Azimuth_in_Deg']
	Zd = smf.drive()['Zenith_Distance_in_Deg']
	img = cam_analyser.get_image_with_blobs_at_Az_Zd(Az, Zd, 4.5)
	resized_img = scale_image_to_apsect4to3_and_rows(img, allsky_rows)
	allsky_stack = hstack((allsky_stack, resized_img))

	over_stack = hstack((over_stack, over))

	info_img = get_star_bar(
		img_desc[0],
		number_of_stars_below_Zd_limit, 
		img_desc[3]['star_count_in_good_night'],
		cam_analyser.moon.shape[0],
		info_rows
	)

	info_stack = hstack((info_stack, info_img))

	print(img_desc[0])

stack = vstack((allsky_stack, info_stack))
stack = vstack((stack, over_stack))

image_name = "all_sky" + '_'+ timestamp() + '.png'
imsave(image_name, stack)